<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('users', function($table) {
            $table->char('innovator_check', 255);
            $table->char('invest_type', 255);
            $table->char('preferred_industry', 255);
            $table->char('average_share_expectancy', 255);
            $table->char('offer_period', 255);
            $table->char('desc_of_innovation', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('innovator_check');
            $table->dropColumn('invest_type');
            $table->dropColumn('preferred_industry');
            $table->dropColumn('average_share_expectancy');
            $table->dropColumn('offer_period');
            $table->dropColumn('desc_of_innovation');
        });
    }
}
