@extends('layouts.app')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
    $auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')

    <div class="dashboard-wrap">
        <div class="container">
            <div id="wrapper">

                @include('admin.menu')

                <div id="page-wrapper">

                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    @if($auth_user->is_admin())
                        <div class="row">

                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="huge">{{$pending_campaign_count}}</div>
                                                <div>@lang('app.pending_campaigns')</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="huge">{{$active_campaign_count}}</div>
                                                <div>@lang('app.active_campaigns')</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="huge">{{$blocked_campaign_count}}</div>
                                                <div>@lang('app.blocked_campaigns')</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="huge">{{$user_count}}</div>
                                                <div>@lang('app.users')</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            @if($pending_campaigns->count() > 0)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        @lang('app.last_pending_campaigns')
                                    </div>
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <th>@lang('app.title')</th>
                                                <th>@lang('app.by')</th>
                                            </tr>

                                            @foreach($pending_campaigns as $pc)
                                                <tr>
                                                    <td>{{$pc->title}}</td>

                                                    <td>{{$pc->user['name']}} <br /> {{$pc->user['email']}} </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            @endif
                                @if($pending_campaigns->count() == 0)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                          Nothing here yet! Make something great!
                                        </div>
                                    </div>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection