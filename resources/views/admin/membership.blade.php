@extends('layouts.app')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')

    <div class="dashboard-wrap">
        <div class="container">
            <div id="wrapper">

                @include('admin.menu')

                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-12">
                            <h1>Membership</h1>
                            <h3>Status: {{$memberStatus}}</h3>

                            @if(Auth::user()->member > 0)
                                <h3>Date of renewal: {{$time}}</h3>
                            @endif
                        </div>
                    </div>
                    @if($memberStatus != "Active" && $memberStatus != "")
                        <table class="table table-bordered">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col">Free</th>
                                <th scope="col">Basic</th>
                                <th scope="col">Silver</th>
                                <th scope="col">Gold</th>
                                <th scope="col">Platinum</th>
                            </tr>
                            </thead>
                            <tbody class="table-striped table-hover" >
                            <tr class="bg-success">
                                <th scope="row">Online & Digital Service</th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                            </tr>
                            <tr>
                                <th scope="row">Search investors</th>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Posting Projects</th>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Promotion</th>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Contacting Buyers</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Monthly Reports</th>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">List Online Member Directory</th>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Email Alerts for latest opportunities</th>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr class="bg-success">
                                <th scope="row">Hands on  service</th>
                                <td></td>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                            </tr>
                            <tr>
                                <th scope="row">Project Roadshows in China</th>
                                <td>£200</td>
                                <td></td>
                                <td>1</td>
                                <td>4</td>
                                <td>Unlimited</td>
                                <td>Unlimited</td>
                            </tr>
                            <tr>
                                <th scope="row">Annual Summit</th>
                                <td>£150</td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Overseas Conference &amp; trade mission (With funding possibility)</th>
                                <td>£5,000</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Project Translation Service</th>
                                <td>£100</td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Access to meeting with Chinese delegations</th>
                                <td>£300</td>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Attend CET events</th>
                                <td>£50</td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr class="bg-success">
                                <th scope="row">China Launch package</th>
                                <td></td>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                            </tr>
                            <tr>
                                <th scope="row">Hotdesk</th>
                                <td>£500</td>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Product Display</th>
                                <td>£300</td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Company set up in China (provide business address & company registration, bank account opening….)</th>
                                <td>£800</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                                <td>&#10004;</td>
                            </tr>
                            <tr>
                                <th scope="row">Ongoing service (Negotiation, product design, shipping…etc)</th>
                                <td>£1,000</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>&#10004;</td>
                            </tr>
                            <tr class="bg-success">
                                <th scope="row">Prices</th>
                                <td></td>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                                <th scope="row"></th>
                            </tr>
                            <tr>

                                <td></td>
                                <td></td>
                                <td>£0 / Per Year</td>
                                <td>£1000 / Per Year</td>
                                <td>£3000 / Per Year</td>
                                <td>£5000 / Per Year</td>
                                <td>£8000 / Per Year</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                                <td></td>
                                <td>
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="L5PMQ3EHZ3HGW">
                                        <input style="width: 100px;" type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                                    </form>
                                </td>
                                <td>
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="8RE4TZTLQBQV4">
                                        <input style="width: 100px;" type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                                    </form>

                                </td>
                                <td>
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="8HEJK3CJ5EC9A">
                                        <input style="width: 100px;" type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                                    </form>
                                </td>
                                <td>
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="E86ZEK3JPEXUA">
                                        <input style="width: 100px;" type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                                    </form>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>
        paypal.Buttons({
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '0.01'
                        }
                    }]
                });
            },
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    alert('Transaction completed by ' + details.payer.name.given_name);
                    // Call your server to save the transaction
                    return fetch('/paypal-transaction-complete', {
                        method: 'post',
                        body: JSON.stringify({
                            orderID: data.orderID
                        })
                    });
                });
            }
        }).render('#paypal-button-container');
    </script>




@endsection

@section('page-js')

@endsection