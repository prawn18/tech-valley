@extends('layouts.app')

@section('content')
    <section class="auth-form">

        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">@lang('app.register')</div>
                        <div class="panel-body">

                            @if(session('error'))
                                <div class="alert alert-danger">
                                    {{session('error')}}
                                </div>
                            @endif

                            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <h2>Contact Details</h2>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name" class="col-md-4 control-label">@lang('app.name')</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">@lang('app.email_address')</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">@lang('app.password')</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="password-confirm" class="col-md-4 control-label">@lang('app.confirm_password')</label>

                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="telephone" class="col-md-4 control-label">Telephone</label>

                                            <div class="col-md-6">
                                                <input id="telephone" type="number" class="form-control" name="telephone" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h2>Company Details</h2>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="company_name" class="col-md-4 control-label">Company Name</label>

                                            <div class="col-md-6">
                                                <input id="company_name" type="text" class="form-control" name="company_name" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="role" class="col-md-4 control-label">Personal Role</label>

                                            <div class="col-md-6">
                                                <input id="role" type="text" class="form-control" name="role" value="{{ old('email') }}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="sector" class="col-md-4 control-label">Industry Sector</label>

                                            <div class="col-md-6">
                                                <input id="sector" type="text" class="form-control" name="sector" value="" required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="address_1" class="col-md-4 control-label">Address 1</label>

                                            <div class="col-md-6">
                                                <input id="address_1" type="text" class="form-control" name="address_1" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="address_2" class="col-md-4 control-label">Address 2</label>

                                            <div class="col-md-6">
                                                <input id="address_2" type="text" class="form-control" name="address_2" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="postcode" class="col-md-4 control-label">Postcode</label>

                                            <div class="col-md-6">
                                                <input id="postcode" type="text" class="form-control" name="postcode" value="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="company_city" class="col-md-4 control-label">City</label>

                                            <div class="col-md-6">
                                                <input id="company_city" type="text" class="form-control" name="company_city" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="innovator_check" class="col-md-4 control-label">Are you an Innovator or Investor?</label>
                                            <div class="col-md-6">
                                                Innovator <input id="innovator_check" style="box-shadow: none" class="form-control" type="radio" name="innovator_check" value="innovator">
                                                Investor <input id="investor_check" style="box-shadow: none" type="radio" class="form-control" name="innovator_check" value="investor" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h2>Investment Details </h2>
                                <p>(Investors Only)</p>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="invest_type" class="col-md-4 control-label">Investment Type</label>

                                            <div class="col-md-6">
                                                <select name="invest_type" id="invest_type">
                                                    <option value="growing_corporates">Growing Corporates</option>
                                                    <option value="financial_investing">Financial Investing</option>
                                                    <option value="mature_enterprises">M&A of Mature Enterprises</option>
                                                    <option value="real_estate_investing">Real Estate Investing</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="preferred_industry" class="col-md-4 control-label">Preferred Industry</label>

                                            <div class="col-md-6">
                                                <input id="preferred_industry" type="text" class="form-control" name="preferred_industry" value="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="investment_amount" class="col-md-4 control-label">Investment Amount</label>

                                            <div class="col-md-6">
                                                <input id="investment_amount" type="number" class="form-control" name="investment_amount" value="" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="average_share_expectancy" class="col-md-4 control-label">Average Share Expectancy </label>

                                            <div class="col-md-6">
                                                <input id="average_share_expectancy" type="number" class="form-control" name="average_share_expectancy" value="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="offer_period" class="col-md-4 control-label">Period Of Offer</label>

                                            <div class="col-md-6">
                                                <input id="offer_period" type="text" class="form-control" name="offer_period" value="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="desc_of_innovation" class="col-md-4 control-label">
                                                Please give a brief description of the innovation you are looking for -
                                                including key aspects, sector, style, location, etc.
                                            </label>

                                            <div class="col-md-6">
                                                <input id="desc_of_innovation" type="text" class="form-control" name="desc_of_innovation" value="" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(get_option('enable_recaptcha_registration') == 1)
                                    <div class="form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="g-recaptcha" data-sitekey="{{get_option('recaptcha_site_key')}}"></div>
                                            @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">
                                            @lang('app.register')
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@if(get_option('enable_recaptcha_registration') == 1)
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endif