<div class="single-donate-wrap">

    <h3 class="campaign-single-sub-title">{{$campaign->short_description}}</h3>

    <div class="single-author-box">
        <img src="{{$campaign->user->get_gravatar()}}">
        <p><strong>{{$campaign->user->name}}</strong> <br />   {{$campaign->address}} </p>
    </div>

    @if($campaign->get_category)
        <div class="campaign-tag-list">
            <a href="#"><i class="glyphicon glyphicon-tag"></i> {{$campaign->get_category->category_name}} </a>
        </div>
    @endif

    <div class="campaign-progress-info">
        <h2>{{ get_amount($campaign->goal) }} @lang('app.goal')</h2>
    </div>
    <div class="campaign-progress-info">
        <h4><strong>{{$campaign->days_left()}}</strong> @lang('app.days_left')</h4>
    </div>

    <div class="campaign-progress-info">
        <h4><strong>Contact Information:</strong>
            @if(Auth::check() && Auth::user()->member > 0)
                @if($user->id == 10 || $user->id == 17)
                    <br> Please email <a href="mailto:support@tech-valley.co.uk">support@tech-valley.co.uk</a> for information quoting this ID:{{$campaign->id}} </h4>
                @else
                    <br> Email: {{$user->email}} <br> Phone: {{$user->phone}} </h4>
                @endif
        @else
            <br> Please upgrade to member to see contact details</h4>
        @endif
    </div>

    <hr />

</div>