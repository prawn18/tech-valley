@extends('layouts.app')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

    <div class="main-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="section-title"> Get Noticed. Get Funded. Grow Faster.</h1>
                    <p class="jumbotron-sub-text"> Tech Approach is a revolutionary bridge for top innovators and investors</p>

                    <div class="jumbotron-button-wrap">
                        <a class="btn btn-lg-outline" href="{{route('browse_categories')}}">@lang('app.support_campaigns')</a>
                        @if(Auth::check() && Auth::user()->member > 0)
                            <a class="btn btn-lg-filled" href="{{route('start_campaign')}}">@lang('app.start_crowdfunding')</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="home-campaign section-bg-white">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">About Us </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="why-choose-us-box">
                        <div class="title">
                            <p>
                                Based in Cambridge, a rapidly growing hi-tech innovation hub, Tech Valley is an online tool for innovators and Investors created by Cambridge Environment and Technology,
                                to connect the most innovative and promising early stage companies with active Investors, advisors, manufactures and corporate professionals in China.
                                We work with the Chinese local government to bring the best opportunities for UK businesses to flourish in China.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home-campaign section-bg-gray"> <!-- explore categories -->
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">@lang('app.explore_categories') </h2>
                </div>
            </div>

            <div class="row">
                @foreach($categories as $cat)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="home-category-box">
                            <img src="{{ $cat->get_image_url() }}" />
                            <div class="title">
                                <a href="{{route('single_category', [$cat->id, $cat->category_slug])}}">{{ $cat->category_name }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="section-footer">
                        <a href="{{route('browse_categories')}}" class="section-action-btn">@lang('app.see_all')</a>
                    </div>
                </div>
            </div>

        </div>
    </section> <!-- #explore categories -->

    @if($staff_picks->count())

        <section class="home-campaign section-bg-white">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-title">@lang('app.staff_picks') </h2>
                    </div>
                </div>

                <div class="row">

                    <div class="box-campaign-lists">

                        @foreach($staff_picks as $spc)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 box-campaign-item">
                                <div class="box-campaign">
                                    <div class="box-campaign-image">
                                        <a href="{{route('campaign_single', [$spc->id, $spc->slug])}}"> <img src="{{ $spc->feature_img_url()}}" /></a>
                                        <div class="overlay">
                                            <a class="info" href="{{route('campaign_single', [$spc->id, $spc->slug])}}">@lang('app.view_details')</a>
                                        </div>
                                    </div>
                                    <div class="box-campaign-content">
                                        <div class="box-campaign-description">
                                            <h4><a href="{{route('campaign_single', [$spc->id, $spc->slug])}}"> {{$spc->title}} </a> </h4>
                                            <p>{{$spc->short_description}}</p>
                                        </div>

                                        <div class="box-campaign-summery">
                                            <h4><strong>{{$spc->days_left()}}</strong> @lang('app.days_left')</h4>
                                        </div>

                                        <div class="box-campaign-footer">
                                            <ul>
                                                <li><img src="{{ asset('assets/images/avatar.png') }}"> John Doe</li>
                                                <li> <strong>$12,000.00</strong> Goal </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach



                    </div>  <!-- #box-campaign-lists -->


                </div>

            </div><!-- /.container -->

        </section>
    @endif




    @if($new_campaigns->count())
        <section class="home-campaign section-bg-white new-home-campaigns">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-title"> @lang('app.new_campaigns') </h2>
                        <div class="why-choose-us-box">
                            <p >Browse our most recent projects that are looking for investment</p>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="box-campaign-lists">

                        @foreach($new_campaigns as $nc)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 box-campaign-item">
                                <div class="box-campaign">
                                    <div class="box-campaign-image">
                                        <a href="{{route('campaign_single', [$nc->id, $nc->slug])}}"><img src="{{ $nc->feature_img_url()}}" /> </a>
                                        <div class="overlay">
                                            <a class="info" href="{{route('campaign_single', [$nc->id, $nc->slug])}}">@lang('app.view_details')</a>
                                        </div>
                                    </div>
                                    <div class="box-campaign-content">
                                        <div class="box-campaign-description">
                                            <h4><a href="{{route('campaign_single', [$nc->id, $nc->slug])}}"> {{$nc->title}} </a> </h4>
                                            <p>{{$nc->short_description}}</p>
                                        </div>
                                        {{--{{$nc}}--}}
                                        <div class="box-campaign-footer">

                                            <h4> <strong>{{get_currency_symbol(get_option('currency_sign'))}}{{number_format($nc->goal)}}</strong> Goal </h4>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>  <!-- #box-campaign-lists -->
                </div>
            </div><!-- /.container -->
        </section>
    @endif

@endsection

@section('page-js')

    <script type="text/javascript">

        $(document).ready(function(){
            $(document).on('click', '.loadMorePagination', function (e) {
                e.preventDefault();
                var anchor = $(this);
                var page_number = anchor.attr('href').split('page=')[1];
                var new_page = parseInt(page_number) + 1;

                //Show Indicator
                $('#load_more_indicator').html('<i class="fa fa-spin fa-spinner"></i>');

                $.get( "{{route('new_campaigns_ajax')}}?page="+page_number, function( data ) {
                    if( ! data.hasOwnProperty('success')){
                        anchor.attr('href',  "{{route('new_campaigns_ajax')}}?page="+new_page);
                        var el = jQuery(data);
                        /* $( ".new-home-campaigns .box-campaign-lists" ).append( el ).masonry( 'appended', el, true ).masonry({
                             itemSelector : '.box-campaign-item'
                         });
                         */
                    }else{
                        anchor.html('@lang('app.no_more_results')');
                    }

                    //Hide
                    $('#load_more_indicator').html('');

                });

            });
        });
    </script>


@endsection