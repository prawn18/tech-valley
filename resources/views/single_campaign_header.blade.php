<div class="single-campaign-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="single-campaign-title">{{$campaign->title}}</h1>
            </div>
        </div>
    </div>
</div>